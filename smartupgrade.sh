#!/bin/bash
# ***************************************************************** 
#                                                                   
# IBM Confidential                                                  
#                                                                   
# OCO Source Materials                                              
#                                                                   
# Copyright IBM Corp. 2009, 2013                                    
#                                                                   
# The source code for this program is not published or otherwise    
# divested of its trade secrets, irrespective of what has been      
# deposited with the U.S. Copyright Office.                         
#                                                                   
# ***************************************************************** 

SYSTEM=`lsb_release -i | tr a-z A-Z`
SUSE="SUSE"
REDHAT="REDHAT"
UBUNTU="UBUNTU"

version="9.0.1" 

#--------------------------------------------------------------------------
# if SUSE or Redhat Platform
#--------------------------------------------------------------------------
if test "$SYSTEM" != "${SYSTEM/$SUSE}" -o "$SYSTEM" != "${SYSTEM/$REDHAT}"
then
	#---------------------------------------------------------
	#uninstall FP if have
	#---------------------------------------------------------
	ls /opt/ibm/notes/fixpack* > /dev/null 2>&1
	if [ $? -eq 0 ];then
		FIXPACK=`rpm -qa | grep ibm_notes_fixpack`
		echo "******** Uninstall $FIXPACK before upgrading ********"
		rpm -e ibm_notes_fixpack
	fi
	ls /opt/ibm/lotus/notes/fixpack* > /dev/null 2>&1
	if [ $? -eq 0 ];then
		FIXPACK=`rpm -qa | grep ibm_lotus_notes_fixpack`
		echo "******** Uninstall $FIXPACK before upgrading ********"
		rpm -e ibm_lotus_notes_fixpack
	fi
    INSTALLED_RPM_NOTES_85x=`rpm -qa | grep ibm_lotus_notes`
	INSTALLED_RPM_NOTES_90=`rpm -qa | grep ibm_notes`
	
    if [ "$INSTALLED_RPM_NOTES_85x" = "" -a "$INSTALLED_RPM_NOTES_90" = "" ]
    then
		#---------------------------------------------------------
		# Install IBM Notes 9.0.1 If Notes was not installed
		#---------------------------------------------------------
        echo " ******** Start to install IBM Notes 9.0.1 ******** "
		echo "Please wait for a while ... "
		components="ibm_notes-9.0.1.i586.rpm ibm_feedreader-9.0.1.i586.rpm ibm_activities-9.0.1.i586.rpm ibm_sametime-9.0.1.i586.rpm ibm_opensocial-9.0.1.i586.rpm"
		echo "rpm -ivh $components"
		rpm -ivh $components
		echo "Installation Finished Successfully."		
    #---------------------------------------------------------
	# Upgrade to dest version of IBM Notes
	#---------------------------------------------------------
	elif [ ! -z "$INSTALLED_RPM_NOTES_85x" ]
	then
	    #---------------------------------------------------------
		# Remove Notes 8.5.x firstly if installed
		#---------------------------------------------------------
		INSTALLED_RPM_NOTES=`rpm -qa | grep ibm_lotus_notes`
		if [ ! -z "$INSTALLED_RPM_NOTES" ]
		then
			INSTALLED_IBM_LOTUS_RPM_PROD=`rpm -qa | grep ibm_lotus_`
			for RPMPROD in $INSTALLED_IBM_LOTUS_RPM_PROD
			do
				RPMTEMP=`echo $RPMPROD |sed 's/-8.*$//g'`	
				RPMCMD="${RPMCMD} ${RPMTEMP}"			
			done
			echo "Start to remove $RPMCMD"
			echo "Please wait for a while ... "
				rpm -e $RPMCMD &>/dev/null
			echo "Upgrade Finished Successfully"
		fi
		#---------------------------------------------------------
		#Install IBM Notes 9.0.1
		#---------------------------------------------------------
        echo " ******** Start to install IBM Notes 9.0.1 ******** "
		echo "Please wait for a while ... "
		components="ibm_notes-9.0.1.i586.rpm ibm_feedreader-9.0.1.i586.rpm ibm_activities-9.0.1.i586.rpm ibm_sametime-9.0.1.i586.rpm ibm_opensocial-9.0.1.i586.rpm"
		echo "rpm -ivh $components"
		rpm -ivh $components
		echo "Installation Finished Successfully."			
    elif [ ! -z "$INSTALLED_RPM_NOTES_90" ]
	then
	    #---------------------------------------------------------
		# Upgrade IBM Notes 9.0.1
		#---------------------------------------------------------
		INSTALLED_IBM_RPM_PROD=`rpm -qa | grep ibm_`

		for RPMPROD in $INSTALLED_IBM_RPM_PROD
		do
			RPMTEMP=`echo $RPMPROD |sed 's/-20[0-9]\{6\}\.[0-9]\{4\}//g'`			
			PACKAGENAME_RPM="$RPMTEMP"."i586.rpm"			
			CURVERSION_RPM=${RPMTEMP##*-}		
			CURPACKAGENAME_RPM=`echo $PACKAGENAME_RPM | sed "s/${CURVERSION_RPM}/${version}/g"`
			
			if test -f "$CURPACKAGENAME_RPM"
			then
				RPMCMD="${RPMCMD} ${CURPACKAGENAME_RPM}"
			else
				if [ ${CURPACKAGENAME_RPM/"ibm_cae"} != ${CURPACKAGENAME_RPM} ]; then
					# uninstall CAE, if it is installed from Notes9.0
					echo "rpm -e ibm_cae-9.0.1.i586"
					rpm -e ibm_cae-9.0.1.i586
				else
					echo "******** Could not find Notes RPM package: $CURPACKAGENAME_RPM********"
					FLAG_RPM="Failed"
				fi
			fi	
		done
		
		if test -z "$FLAG_RPM"
		then
			echo "rpm -Uvh $RPMCMD"
			rpm -Uvh $RPMCMD
			if [  "${RPMCMD/'ibm_feedreader'}"  =  "${RPMCMD}"  ]; then
				echo "rpm -ivh ibm_feedreader*.rpm"
				rpm -ivh ibm_feedreader*.rpm
			fi
			if [  "${RPMCMD/'ibm_opensocial'}" = "${RPMCMD}"  ]; then
				echo "rpm -ivh ibm_opensocial*.rpm"
				rpm -ivh ibm_opensocial*.rpm
		        fi
			echo "******** Upgrade Finished Successfully ********"
		else
			echo "******** Upgrade Failed ********"
		fi
	fi
#--------------------------------------------------------------------------
# if UBUNTU Platform
#--------------------------------------------------------------------------	
elif test "$SYSTEM" != "${SYSTEM/$UBUNTU}"
then
	#---------------------------------------------------------
	#uninstall FP if have
	#---------------------------------------------------------
	ls /opt/ibm/notes/fixpack* > /dev/null 2>&1
	if [ $? -eq 0 ];then
		FIXPACK=`dpkg -l | grep ibm-notes-fixpack | awk '{print $2}'`
		echo "******** Uninstall $FIXPACK before upgrading ********"
		dpkg -r ibm-notes-fixpack
		dpkg --purge ibm-notes-fixpack
	fi
	ls /opt/ibm/lotus/notes/fixpack* > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		FIXPACK=`dpkg -l | grep ibm-lotus-notes-fixpack | awk '{print $2}'`
		echo "******** Uninstall $FIXPACK before upgrading ********"
		dpkg -P ibm-lotus-notes-fixpack
		dpkg --purge ibm-lotus-notes-fixpack
	fi
	INSTALLED_DEB_NOTES_85x=`dpkg -l ibm-lotus-notes | grep ii`
	INSTALLED_DEB_NOTES_90=`dpkg -l ibm-notes | grep ii`

    if [ "$INSTALLED_DEB_NOTES_85x" = "" -a "$INSTALLED_DEB_NOTES_90" = "" ]
    then
		#---------------------------------------------------------
		# Install IBM Notes 9.0.1 If Notes was not installed
		#---------------------------------------------------------
        echo " ******** Start to install IBM Notes 9.0.1 ******** "
		echo "Please wait for a while ... "
		components="ibm-feedreader-9.0.1.i586.deb ibm-activities-9.0.1.i586.deb ibm-sametime-9.0.1.i586.deb ibm-opensocial-9.0.1.i586.deb"
		echo "dpkg -i ibm-notes-9.0.1.i586.deb"
		dpkg -i ibm-notes-9.0.1.i586.deb	
		echo "dpkg -i $components"
		dpkg -i $components
		echo "Installation Finished Successfully."	
	#---------------------------------------------------------
	# start to upgrade current Notes Family products
	#---------------------------------------------------------
	elif [ ! -z "$INSTALLED_DEB_NOTES_85x" ]
	then
		#---------------------------------------------------------
		# Remove Notes 8.5.x firstly if installed
		#---------------------------------------------------------
		echo " ******** Removing previous version ******** "	
		components="ibm-lotus-notes ibm-lotus-cae ibm-lotus-activities ibm-lotus-feedreader ibm-lotus-symphony ibm-lotus-sametime ibm-lotus-fixpack ibm-lotus-hotfix"
		dpkg -P $components &>/dev/null
		#--------------------------------------------------------
		#Install IBM Notes 9.0.1
		#---------------------------------------------------------
        echo " ******** Start to install IBM Notes 9.0.1 ******** "
		echo "Please wait for a while ... "
		components="ibm-feedreader-9.0.1.i586.deb ibm-activities-9.0.1.i586.deb ibm-sametime-9.0.1.i586.deb ibm-opensocial-9.0.1.i586.deb"
		echo "dpkg -i ibm-notes-9.0.1.i586.deb"
		dpkg -i ibm-notes-9.0.1.i586.deb
		echo "dpkg -i $components"	
		dpkg -i $components
		echo "Installation Finished Successfully."	
		
	elif [ ! -z "$INSTALLED_DEB_NOTES_90" ]
	then
	    INSTALLED_IBM_PROD=`dpkg -l | grep ibm- | awk '{print $2}'`
		for STRPROD in $INSTALLED_IBM_PROD
		do
			PACKAGENAME_DEB="$STRPROD-${version}"."i586.deb"
			UNDERLINESTYLE=`echo $STRPROD | sed "s/-/_/g"`			
			PACKAGENAME_DEB_UNDERLINE="$UNDERLINESTYLE-${version}"."i586.deb"

			if test -f "$PACKAGENAME_DEB" 
			then
				DEBCMD="${DEBCMD} ${PACKAGENAME_DEB}"
			elif test -f "$PACKAGENAME_DEB_UNDERLINE"	
			then
				DEBCMD="${DEBCMD} ${PACKAGENAME_DEB_UNDERLINE}"
			else
				if [  ${PACKAGENAME_DEB/"ibm-cae"} != $PACKAGENAME_DEB -o ${PACKAGENAME_DEB_UNDERLINE/"ibm_cae"} != ${PACKAGENAME_DEB_UNDERLINE} ]; then
					echo "dpkg -P ibm-cae"
					dpkg -P ibm-cae
				else
					echo "******** Could not find Notes DEB package: $PACKAGENAME_DEB********"
					FLAG_DEB="Failed"
				fi
			fi	
		done
		#---------------------------------------------------
		#trim string DEBCMD 
		#---------------------------------------------------
		DEBCMD_TRIM=`echo $DEBCMD | sed "s/^ *//g"`
		
		if test -z $FLAG_DEB
		then
			NOTESVERSION="notes-"$version
			#---------------------------------------------------
			#Upgrade Notes first
			#---------------------------------------------------
			NOTES_DEB=`echo ${DEBCMD_TRIM}| sed "s/.*\(ibm.*notes-9.0[^ ]*\).*/\1/g"`
			FEED_DEB=`echo ${DEBCMD_TRIM}| sed -n "s/.*\(ibm.*feedreader-9.0[^ ]*\).*/\1/gp"`
			OPENSOCIAL_DEB=`echo ${DEBCMD_TRIM}| sed -n "s/.*\(ibm.*opensocial-9.0[^ ]*\).*/\1/gp"`
			echo "dpkg -i ${NOTES_DEB}"
			dpkg -i $NOTES_DEB
			if [ $? -ne 0 ]
			then
				 FLAG_DEB="Failed"
			fi 
			#---------------------------------------------------
			#if Activities installed
			#---------------------------------------------------
			ACTIVITIES_DEB=`echo ${DEBCMD_TRIM}| sed "s/.*\(ibm.*activities-9.0[^ ]*\).*/\1/g"`
			if test "${ACTIVITIES_DEB}" != "${DEBCMD_TRIM}"
			then
				echo "dpkg -i $ACTIVITIES_DEB"
				dpkg -i $ACTIVITIES_DEB
				if [ $? -ne 0 ]
				then
					FLAG_DEB="Failed"
				fi	
				DEBCMD_TRIM="${DEBCMD_TRIM/$ACTIVITIES_DEB}"
				DEBCMD_TRIM=`echo $DEBCMD_TRIM | sed "s/^ *//g"`
			fi



			#---------------------------------------------------
			#Other components including NL packages
			#---------------------------------------------------
			DEBCMD_TRIM="${DEBCMD_TRIM/$NOTES_DEB}"
			DEBCMD_TRIM="${DEBCMD_TRIM/$OPENSOCIAL_DEB}"
#			if [ ! -z "$DEBCMD_TRIM" ];then
				if [ "$OPENSOCIAL_DEB" != "" ]; then
					dpkg -P ibm-opensocial
				fi
				DEBCMD_TRIM="${DEBCMD_TRIM/$FEED_DEB}"
				echo "dpkg -i $DEBCMD_TRIM ibm-feedreader*.deb"
				dpkg -i $DEBCMD_TRIM ibm-feedreader*.deb 
				echo "dpkg -i ibm-opensocial*.deb"
				dpkg -i ibm-opensocial*.deb	
				if [ $? -ne 0 ]; then
					FLAG_DEB="Failed"
				fi	
#			else
#				echo "******** no other component installed ********"
#			fi
			if [ -z "$FLAG_DEB" ]; then
				echo "******** Smart Upgrade Finished Successfully ********"
			else
				echo "******** Smart Upgrade Finished with error ********"	
			fi	
		else
			echo "******** Smart Upgrade Failed ********"
		fi
    fi
	dpkg --configure -a
else
    echo " ******** Could not support smart upgrade on $SYSTEM ******** "
fi
